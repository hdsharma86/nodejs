const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/task-manager-api', {
    useNewUrlParser: true,
    useCreateIndex: true
}).then(() => {
    console.log('Connection established!');
}).catch((e) => {
    console.log('Connection error!');
});