const express = require('express');
require('./db/mongoose');
const User = require('./models/user');
const Task = require('./models/task');

const app = express();
app.use(express.json());
const port = process.env.PORT || 3000;

app.post('/users', (req, res) => {
    if(req.body){
        const user = new User(req.body);
        user.save().then(() => {
            res.status(201).send(user);
        }).catch((error) => {
            res.status(400).send(error);
        });
    }
});

app.get('/users', (req, res) => {
    User.find({}).then((result) => {
        res.status(200).send(result);
    }).catch((error) => {
        res.status(500).send(error);
    });
});

app.get('/users/:id', (req, res) => {
    const _id = req.params.id;
    if(_id){
        User.findById(_id).then((user) => {
            if(!user){
                res.status(404).send();
            } 
            res.status(200).send(user);
        }).catch((error) => {
            res.status(500).send(error);
        });
    }
});

app.post('/tasks', (req, res) => {
    if(req.body){
        const task = new Task(req.body);
        task.save().then(()=>{
            res.status(201).send(task);
        }).catch((e) => {
            res.status(400).send(e);
        });
    }
});

app.get('/tasks', (req, res) => {
    Task.find({}).then((result) => {
        res.status(200).send(result);
    }).catch((error) => {
        res.status(500).send(error);
    });
});

app.get('/tasks/:id', (req,res) => {
    const _id = req.params.id;
    if(!_id){
        res.status(401).send();
    }
    Task.findById(_id).then((task) => {
        res.status(200).send(task);
    }).catch((error) => {
        res.status(500).send(error);
    });
});

app.listen(port, () => {
    console.log('Server is listening on port '+port);
});